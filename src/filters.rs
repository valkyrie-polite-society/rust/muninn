use crate::mongodb_client::MongoClient;
use evmap::{ReadHandle, ReadHandleFactory};
use std::convert::Infallible;
use warp::Filter;

pub fn with_mongodb_client(client: MongoClient) -> impl Filter<Extract = (MongoClient,), Error = Infallible> + Clone {
  warp::any().map(move || client.clone())
}

pub fn with_config(factory: ReadHandleFactory<String, String>) -> impl Filter<Extract = (ReadHandle<String, String>,), Error = Infallible> + Clone {
  warp::any().map(move || factory.handle())
}
