use anyhow::Result;
use config::{Config, File};
use evmap::WriteHandle;
use std::collections::HashMap;
use std::env;
use std::path::Path;
use walkdir::WalkDir;
use tokio::signal::unix::{signal, SignalKind};
use std::sync::Arc;
use tokio::sync::Notify;

pub async fn update_on_sighup(cfg: WriteHandle<String, String>, stop: Arc<Notify>) {
  let mut sighup = signal(SignalKind::hangup()).unwrap();
  let mut cfg = cfg;

  loop {
    tokio::select! {
      Some(_) = sighup.recv() => {
        let _ = refresh(&mut cfg)
          .map_err(|e| format!("error refreshing config: {:?}", e));
      }
      _ = stop.notified() => {
        println!("Hangup handler stopping");
        return;
      }
    }
  }
}

fn refresh(write: &mut WriteHandle<String, String>) -> Result<()> {
  load_config().map(|config| {
    config.iter().for_each(|(k, v)| {write.insert(k.to_owned(), v.to_owned());});
    write.refresh();
  })
}

fn load_config() -> Result<HashMap<String, String>> {
  let mut settings = Config::default();

  load_directory("/etc/muninn", &mut settings)?;

  // Read all files from the NOMAD_SECRETS_DIR, if any.
  if let Some(env_var) = env::var_os("NOMAD_SECRETS_DIR") {
    if let Ok(path) = env_var.into_string() {
      load_directory(path, &mut settings)?;
    }
  }

  let config = settings.try_into::<HashMap<String, String>>()?;
  Ok(config)
}

fn load_directory<P: AsRef<Path>>(path: P, settings: &mut Config) -> Result<()> {
  for file in WalkDir::new(path).into_iter().filter_map(|e| e.ok()) {
    let metadata = file.metadata()?;
    if metadata.is_file() {
      settings.merge(File::from(file.path()))?;
    }
  }

  Ok(())
}
