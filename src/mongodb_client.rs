use crate::cli;
use anyhow::Result;
use mongodb::{Client, Collection};
use mongodb::bson::doc;
use mongodb::results::*;
use mongodb::options::*;
use serde::{Serialize, de::DeserializeOwned};
use std::str::FromStr;

pub async fn from_options(options: &cli::Options) -> Result<MongoClient> {
  let uri = options.mongodb_connection_url.as_str();
  let resolver_config = options.mongodb_dns_resolver.clone();
  let mongodb_options = ClientOptions::parse_with_resolver_config(uri, resolver_config.inner).await?;

  Ok(MongoClient {
    inner: Client::with_options(mongodb_options).unwrap()
  })
}

#[derive(Clone)]
pub struct MongoClient {
  inner: mongodb::Client
}

impl MongoClient {
  pub async fn save<T: Serialize>(self, database: &str, collection: &str, document: &T) -> Result<InsertOneResult> {
    let db = self.inner.database(database);
    let coll: Collection<T> = db.collection(collection);

    let options = InsertOneOptions::builder()
      .bypass_document_validation(false)
      .build();

    Ok(coll.insert_one(document, options).await?)
  }

  pub async fn retrieve<T: DeserializeOwned + Unpin + Send + Sync>(self, database: &str, collection: &str, id: &str) -> Result<Option<T>> {
    let db = self.inner.database(database);
    let coll: Collection<T> = db.collection(collection);
    let oid = mongodb::bson::oid::ObjectId::from_str(id)?;

    let filter = doc! {
     "_id": oid,
    };

    let options = FindOneOptions::builder()
      .build();

    Ok(coll.find_one(Some(filter), options).await?)
  }
}
