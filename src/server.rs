use evmap::ReadHandleFactory;
use warp::Filter;
use crate::estate::routes::*;
use crate::filters::*;
use std::future::Future;
use std::sync::Arc;
use sd_notify::NotifyState;
use tokio::sync::Notify;
use crate::mongodb_client::MongoClient;
use tokio::signal::unix::{signal, SignalKind};

pub fn new(
  address: std::net::Ipv4Addr,
  port: u16, stop: Arc<Notify>,
  factory: ReadHandleFactory<String, String>,
  client: MongoClient
) -> impl Future<Output = ()> + 'static
{
  let healthz = warp::path!("healthz")
  .map(health);
  let add_estate_route = warp::path!("estates")
    .and(warp::post())
    .and(with_mongodb_client(client.clone()))
    .and(warp::body::json())
    .and_then(add_estate);
  let get_estate_route = warp::path!("estates")
    .and(warp::get())
    .and(with_mongodb_client(client))
    .and(with_config(factory))
    .and(warp::path::param::<String>())
    .and_then(get_estate);

  let routes = healthz
    .or(add_estate_route)
    .or(get_estate_route);

  let (_, server) = warp::serve(routes)
    .bind_with_graceful_shutdown((address, port), warp_shutdown(stop));

  server
}

async fn warp_shutdown(shutdown: Arc<Notify>) {
  println!("Server started");
  let _ = sd_notify::notify(true, &[NotifyState::Ready, NotifyState::Status(String::from("serving"))]);
  shutdown.notified().await;
  let _ = sd_notify::notify(true, &[NotifyState::Stopping, NotifyState::Status(String::from("gracefully shutting down"))]);
  println!("Gracefully shutting down");
}

pub async fn handle_terminate_or_interrupt(stop: Arc<Notify>) {
  let mut sigint = signal(SignalKind::interrupt()).unwrap();
  let mut sigterm = signal(SignalKind::terminate()).unwrap();

  tokio::select! {
    Some(_) = sigint.recv() => {
      stop.notify_waiters();
    }
    Some(_) = sigterm.recv() => {
      stop.notify_waiters();
    }
  }
}
