use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct NewEstate {
  name: String
}

#[derive(Serialize, Deserialize)]
pub struct Estate {
  _id: String,
  name: String
}
