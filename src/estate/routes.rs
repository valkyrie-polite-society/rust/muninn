use warp::{http::StatusCode, Rejection, Reply};
use crate::estate::types::*;
use crate::mongodb_client::MongoClient;
use evmap::ReadHandle;
use serde::Serialize;
use warp::reject::Reject;

pub async fn add_estate(
  client: MongoClient,
  estate: NewEstate
) -> Result<impl Reply, Rejection> {
  client.save("application", "estates", &estate).await
    .map(|_| StatusCode::CREATED)
    .map_err(|_e| todo!())
}

pub async fn get_estate(
  client: MongoClient,
  _config: ReadHandle<String, String>,
   id: String
  ) -> Result<impl Reply, Rejection> {
  let maybe_estate = client.retrieve::<Estate>("application", "estates", &id).await;
  
  json_from_result_option(maybe_estate)
}

pub fn health() -> String {
  String::from("Ok")
}

fn json_from_result_option<T: Serialize>(result: Result<Option<T>, anyhow::Error>) -> Result<impl Reply, Rejection> {
  match result {
    Ok(Some(value)) => Ok(warp::reply::json(&value)),
    Ok(None)        => Err(warp::reject::not_found()),
    Err(error)      => Err(warp::reject::custom(AnyhowError{inner: error}))
  }
}

#[allow(dead_code)]
#[derive(Debug)]
struct AnyhowError {
  inner: anyhow::Error
}

impl Reject for AnyhowError {
}
