use clap::Parser;
use std::net::Ipv4Addr;
use std::str::FromStr;
use mongodb::options::ResolverConfig;

#[derive(Parser)]
#[clap(version = "1.0", author = "Adrienne Cohea <adriennecohea@gmail.com>")]
pub struct Options {
    #[clap(long, /*env = "BIND_ADDRESS",*/ default_value = "127.0.0.1")] pub bind_address: Ipv4Addr,
    #[clap(long, /*env = "PORT",*/ default_value = "4000")] pub port: u16,
    #[clap(long, /*env = "MONGODB_CONNECTION_URL",*/ default_value = "mongodb://localhost:27017")] pub mongodb_connection_url: String,
    #[clap(long, /*env = "MONGODB_DNS_RESOLVER",*/ default_value = "google")] pub mongodb_dns_resolver: MyResolverConfig,
}

#[derive(Clone)]
pub struct MyResolverConfig {
  pub inner: ResolverConfig
}

impl FromStr for MyResolverConfig {
  type Err = String;
  fn from_str(input: &str) -> Result<Self, <Self as FromStr>::Err> {
    match input.to_lowercase().as_str() {
      "cloudflare" => Ok(MyResolverConfig{inner: ResolverConfig::cloudflare()}),
      "quad9" => Ok(MyResolverConfig{inner: ResolverConfig::quad9()}),
      "google" => Ok(MyResolverConfig{inner: ResolverConfig::google()}),
      _ => Err(format!("Invalid MongoDB DNS resolver: {}", input))
    }
  }
}
