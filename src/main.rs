extern crate bson;
extern crate clap;
extern crate serde;
extern crate tracing;

mod server;
mod cli;
mod config;
mod filters;
mod mongodb_client;
mod estate;
use clap::Parser;
use anyhow::Result;
use tokio::sync::Notify;
use std::sync::Arc;

#[tokio::main]
async fn main() -> Result<()> {
  tracing_subscriber::fmt::init();
  let options = cli::Options::parse();
  let client = mongodb_client::from_options(&options).await?;
  let (read_handle, write_handle) = evmap::new();
  let notify = Arc::new(Notify::new());
  let server = server::new(options.bind_address, options.port, notify.clone(), read_handle.factory(), client);

  let _ = tokio::join!(
    tokio::spawn(server),
    tokio::spawn(server::handle_terminate_or_interrupt(notify.clone())),
    tokio::spawn(config::update_on_sighup(write_handle, notify.clone())),
  );

  Ok(())
}
