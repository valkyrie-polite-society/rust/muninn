FROM adriennecohea/rust-musl-builder:1.56-nightly as builder

RUN USER=root cargo new --bin muninn
WORKDIR /home/rust/src/muninn
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
RUN cargo build --release
RUN rm src/*.rs

ADD . ./

RUN rm ./target/x86_64-unknown-linux-musl/release/deps/muninn*
RUN cargo build --release


FROM alpine:latest

ARG BIN=/usr/bin

EXPOSE 4000

ENV TZ=Etc/UTC

RUN apk update \
    && apk add --no-cache ca-certificates tzdata \
    && rm -rf /var/cache/apk/*

COPY --from=builder /home/rust/src/muninn/target/x86_64-unknown-linux-musl/release/muninn ${BIN}/muninn

CMD ["/usr/bin/muninn"]
